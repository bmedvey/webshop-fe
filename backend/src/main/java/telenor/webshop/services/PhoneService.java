package telenor.webshop.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import telenor.webshop.models.Phone;
import telenor.webshop.models.PhoneCategories;

import java.util.*;

@Service
public class PhoneService {
    private final MongoTemplate mongoTemplate;

    @Autowired
    public PhoneService(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    public Criteria createCriteria(String key, String filters) {
        ArrayList<String> arrayOfFilters = new ArrayList<>();
        Arrays.stream(filters.split(",")).forEach(filter -> arrayOfFilters.add(filter));

        return Criteria.where(key).in(arrayOfFilters);
    }

    public Map<String, Object> getPhonesWithSpecificData(Optional<String> memorySize, Optional<String> simType,
                                                 Optional<String> manufacturer, Optional<String> series,
                                                 int pageNumber, int pageSize) {
        Query query = new Query();
        Criteria criteria = new Criteria();
        List<Criteria> andCriterias = new ArrayList<>();
        Map<String, Object> returnValue = new HashMap<>();

        if(memorySize.isPresent() && !memorySize.get().equals("")) {
            andCriterias.add(this.createCriteria("memorySize", memorySize.get()));
        }
        if(simType.isPresent() && !simType.get().equals("")) {
            andCriterias.add(this.createCriteria("simType", simType.get()));
        }
        if(manufacturer.isPresent() && !manufacturer.get().equals("")) {
            andCriterias.add(this.createCriteria("manufacturer", manufacturer.get()));
        }
        if(series.isPresent()&& !series.get().equals("")) {
            andCriterias.add(this.createCriteria("series", series.get()));
        }

        if (!andCriterias.isEmpty()) {
            criteria.andOperator(andCriterias.toArray(new Criteria[andCriterias.size()]));
            query.addCriteria(criteria);
        }

        query.skip(pageNumber * pageSize);
        query.limit(pageSize);

        List<Phone> phoneList = mongoTemplate.find(query, Phone.class,"phones");

        // Remove the image and the detail property for the phone list, these are only needed for the phonePage
        phoneList.stream().forEach(phone -> {
            phone.setDescription(null);
            phone.setImage(null);
        });

        returnValue.put("phoneList", phoneList);

        // count the filtered phones, for the pagination
        int phoneCount = (int) mongoTemplate.count(query, "phones");
        // count the pagesize with phoneCount / pageSize
        // if the phoneCount can fit exactly on the pages then return the result - 1 otherwise return the result
        int pageCount = phoneCount % pageSize == 0 ? phoneCount / pageSize - 1 : phoneCount / pageSize;
        returnValue.put("pageCount", pageCount);

        return returnValue;
    }

    public PhoneCategories getCategories() {
        List<Phone> phones = mongoTemplate.findAll(Phone.class, "phones");
        PhoneCategories categories = new PhoneCategories();

        phones.stream().forEach(phone -> categories.addPhoneToCategories(phone));

        return categories;
    }

    public Phone getPhone(String id) {
        Criteria criteria = Criteria.where("id").is(id);
        Query query = Query.query(criteria);

        Phone phone = mongoTemplate.findOne(query, Phone.class,"phones");
        // Remove the thumbnail, we don't need that for the phonePage only for the list of phones onthe homePage
        if (phone != null) {
            phone.setThumbnail(null);
            return phone;
        }

        return null;
    }
}
