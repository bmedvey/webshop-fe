package telenor.webshop.models;

import lombok.Data;
import java.util.*;

@Data
public class PhoneCategories {
    private List<Manufacturer> manufacturerList = new ArrayList<>();
    private Set<Sim> simTypes = new HashSet<>();
    private Set<Memory> memorySizes = new HashSet<>();

    public void addPhoneToCategories(Phone phone) {
        String series = phone.getSeries();

        Manufacturer manufacturer = new Manufacturer();
        String manufacturerName = phone.getManufacturer();
        manufacturer.addSeries(series);
        manufacturer.setManufacturer(manufacturerName);

        if (this.hasManufacturerName(manufacturer.getManufacturer())) {
            this.appendManufacturerList(manufacturer, series);
        } else {
            manufacturerList.add(manufacturer);
        }

        simTypes.add(phone.getSimType());
        memorySizes.add(phone.getMemorySize());
    }

    public boolean hasManufacturerName(String manufacturerName) {
        return this.manufacturerList.stream().filter(m -> manufacturerName.equals(m.getManufacturer())).count() == 1 ? true : false;
    }

    public Manufacturer getManufacturer(Manufacturer manufacturer) {
        return this.manufacturerList.stream().filter(m -> manufacturer.getManufacturer().equals(m.getManufacturer())).findAny().orElse(null);
    }

    public void appendManufacturerList(Manufacturer manufacturer, String series) {
        this.getManufacturer(manufacturer).addSeries(series);
    }
}
