package telenor.webshop.models;

public enum Memory {
    SMALL,
    MEDIUM,
    LARGE,
    EXTRALARGE;
}
