package telenor.webshop.models;

import lombok.Data;
import org.bson.types.Binary;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "phones")
public class Phone {
    @Id
    private String id;
    private String manufacturer;
    private String series;
    private String model;
    private String description;
    private Sim simType;
    private Memory memorySize;
    private Binary image;
    private Binary thumbnail;
    private int price;
}
