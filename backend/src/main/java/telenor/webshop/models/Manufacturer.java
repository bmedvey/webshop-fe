package telenor.webshop.models;

import lombok.Data;

import java.util.HashSet;
import java.util.Set;

@Data
public class Manufacturer {
    private String manufacturer;
    private Set<String> series = new HashSet<>();

    public void addSeries(String series) {
        this.series.add(series);
    }
}
