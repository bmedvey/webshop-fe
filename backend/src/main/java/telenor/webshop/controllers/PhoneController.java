package telenor.webshop.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import telenor.webshop.models.Phone;
import telenor.webshop.models.PhoneCategories;
import telenor.webshop.services.PhoneService;

import java.util.Map;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping
public class PhoneController {

    private final PhoneService phoneService;

    @Autowired
    public PhoneController(PhoneService phoneService) {
        this.phoneService = phoneService;
    }

    @GetMapping(value = "/phone/{id}", produces = {"application/json"})
    public ResponseEntity<Phone> getPhone(@PathVariable("id") String id) {
        return ResponseEntity.ok(phoneService.getPhone(id));
    }

    @GetMapping(path="/phones",
            produces = {"application/json"})
    public ResponseEntity<Map<String, Object>> getPhones(
            @RequestParam("memorySize") Optional<String> memorySize,
            @RequestParam("simType") Optional<String> simType,
            @RequestParam("manufacturer") Optional<String> manufacturer,
            @RequestParam("series") Optional<String> series,
            @RequestParam("pageNumber") Optional<Integer> pageNumber,
            @RequestParam("pageSize") Optional<Integer> pageSize
    ) {
        return ResponseEntity.ok(phoneService.getPhonesWithSpecificData(
                memorySize,
                simType,
                manufacturer,
                series,
                pageNumber.orElse(0),
                pageSize.orElse(5))
        );
    }

    @GetMapping(path="/categories")
    public ResponseEntity<PhoneCategories> getCategories() {
        return ResponseEntity.ok(phoneService.getCategories());
    }
}
