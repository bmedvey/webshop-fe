package telenor.webshop.models;

import com.mscharhag.oleaster.runner.OleasterRunner;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.Collections;

import static com.mscharhag.oleaster.matcher.Matchers.expect;
import static com.mscharhag.oleaster.runner.StaticRunnerSupport.*;

@RunWith(OleasterRunner.class)
public class PhoneCategoriesUnitTest {
    private PhoneCategories phoneCategories;
    private String mockManufacturerName = "mockManufacturer";

    {
        beforeEach(() -> {
            phoneCategories = new PhoneCategories();
        });

        describe("PhoneCategories", () -> {
            describe("addPhoneToCategories", () -> {
                it("should add details of the phone to the category", () -> {
                    // Init
                    String mockSeries = "mockSeries";
                    Memory mockMemory = Memory.SMALL;
                    Sim mockSimType = Sim.Dual;

                    Phone mockPhone = new Phone();
                    mockPhone.setManufacturer(mockManufacturerName);
                    mockPhone.setSeries(mockSeries);
                    mockPhone.setMemorySize(Memory.SMALL);
                    mockPhone.setSimType(Sim.Dual);

                    Manufacturer mockManufacturer = new Manufacturer();
                    mockManufacturer.setManufacturer(mockManufacturerName);
                    mockManufacturer.setSeries(Collections.singleton(mockSeries));

                    // Action
                    phoneCategories.addPhoneToCategories(mockPhone);

                    // Test
                    expect(phoneCategories.getManufacturerList()).toNotBeEmpty();
                    expect(phoneCategories.getManufacturerList()).toEqual(Arrays.asList(mockManufacturer));
                    expect(phoneCategories.getMemorySizes()).toEqual(Collections.singleton(mockMemory));
                    expect(phoneCategories.getSimTypes()).toEqual(Collections.singleton(mockSimType));
                });
            });

            describe("hasManufacturerName", () -> {
                it("should return false if the manufactuter is not added to the manufacturerList", () -> {
                    // Init

                    // Action
                    Boolean result = phoneCategories.hasManufacturerName(mockManufacturerName);

                    // Test
                    expect(result).toEqual(false);
                });

                it("should return true if the manufactuter is already added to the manufacturerList", () -> {
                    // Init
                    Manufacturer mockManufacturer = new Manufacturer();
                    mockManufacturer.setManufacturer(mockManufacturerName);

                    phoneCategories.setManufacturerList(Arrays.asList(mockManufacturer));

                    // Action
                    Boolean result = phoneCategories.hasManufacturerName(mockManufacturerName);

                    // Test
                    expect(result).toEqual(true);
                });
            });

            describe("getManufacturer", () -> {
                it("should return the manufactuter if the manufacturerList contains it", () -> {
                    // Init
                    Manufacturer mockManufacturer = new Manufacturer();
                    mockManufacturer.setManufacturer(mockManufacturerName);

                    phoneCategories.setManufacturerList(Arrays.asList(mockManufacturer));

                    // Action
                    Manufacturer result = phoneCategories.getManufacturer(mockManufacturer);

                    // Test
                    expect(mockManufacturer).toEqual(result);
                });

                it("should return true if the manufactuter is not added to the manufacturerList", () -> {
                    // Init

                    // Action
                    Manufacturer result = phoneCategories.getManufacturer(new Manufacturer());

                    // Test
                    expect(result).toEqual(null);
                });
            });

            describe("appendManufacturerList", () -> {
                it("should addend the manufacturer list with the given series", () -> {
                    // Init
                    Manufacturer mockManufacturer = new Manufacturer();
                    mockManufacturer.setManufacturer(mockManufacturerName);

                    phoneCategories.setManufacturerList(Arrays.asList(mockManufacturer));

                    // Action
                    phoneCategories.appendManufacturerList(mockManufacturer, "mockSeries");

                    mockManufacturer.addSeries("mockSeries");

                    // Test
                    expect(phoneCategories.getManufacturerList()).toEqual(Arrays.asList(mockManufacturer));
                });
            });
        });
    }
}
