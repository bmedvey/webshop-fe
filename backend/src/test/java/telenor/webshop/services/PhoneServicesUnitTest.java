package telenor.webshop.services;

import com.mscharhag.oleaster.runner.OleasterRunner;
import org.bson.Document;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import telenor.webshop.models.Phone;

import java.util.*;

import static com.mscharhag.oleaster.matcher.Matchers.expect;
import static com.mscharhag.oleaster.runner.StaticRunnerSupport.beforeEach;
import static com.mscharhag.oleaster.runner.StaticRunnerSupport.describe;
import static com.mscharhag.oleaster.runner.StaticRunnerSupport.it;
import static org.mockito.Mockito.*;

@RunWith(OleasterRunner.class)
public class PhoneServicesUnitTest {
    private PhoneService phoneService;
    private MongoTemplate mockMongoTemplate;

    {
        beforeEach(() -> {
            mockMongoTemplate = mock(MongoTemplate.class);

            phoneService = spy(new PhoneService(mockMongoTemplate));
        });

        describe("PhoneService", () -> {
            describe("createCriteria", () -> {
                it("should create criteria with key and one filter", () -> {
                    // Init
                    String mockKey = "mockKey";
                    String mockFilters = "mockFilter1";

                    // Action
                    Criteria result = phoneService.createCriteria(mockKey, mockFilters);

                    // Test
                    Document expectedDocumet = new Document();
                    expectedDocumet.append("$in", Arrays.asList("mockFilter1"));

                    expect(result.getKey()).toEqual(mockKey);
                    expect(result.getCriteriaObject().get(mockKey)).toEqual(expectedDocumet);
                });

                it("should create criteria with key and multiple filters", () -> {
                    // Init
                    String mockKey = "mockKey";
                    String mockFilters = "mockFilter1,mockFilter2";

                    // Action
                    Criteria result = phoneService.createCriteria(mockKey, mockFilters);

                    // Test
                    Document expectedDocumet = new Document();
                    expectedDocumet.append("$in", Arrays.asList("mockFilter1", "mockFilter2"));

                    expect(result.getKey()).toEqual(mockKey);
                    expect(result.getCriteriaObject().get(mockKey)).toEqual(expectedDocumet);
                });

            });
            describe("getPhonesWithSpecificData", () -> {
                it("should check the query send to the mongo and the number of calls as well", () -> {
                    // Init
                    Query mockQuery = new Query();
                    List<Criteria> andCriterias = new ArrayList<>();
                    Criteria criteria = new Criteria();
                    andCriterias.add(Criteria.where("simType").in(Arrays.asList("Dual")));
                    criteria.andOperator(andCriterias.toArray(new Criteria[andCriterias.size()]));
                    mockQuery.addCriteria(criteria);

                    // Action
                    phoneService.getPhonesWithSpecificData(Optional.empty(), Optional.of("Dual"),
                                    Optional.empty(), Optional.empty(), 0,5);

                    // Test
                    ArgumentCaptor<Query> queryArgumentCaptor = ArgumentCaptor.forClass(Query.class);

                    verify(mockMongoTemplate, times(1))
                            .find(queryArgumentCaptor.capture(), eq(Phone.class), eq("phones"));
                    verify(mockMongoTemplate, times(1))
                            .count(any(), anyString());
                    expect(queryArgumentCaptor.getValue().getQueryObject()).toEqual(mockQuery.getQueryObject());
                });
            });

            describe("getCategories", () -> {
                it("should check the callCount of the mongo.findall method", () -> {
                    // Init

                    // Action
                    phoneService.getCategories();

                    // Test
                    verify(mockMongoTemplate, times(1)).findAll(Phone.class, "phones");
                });
            });

            describe("getPhone", () -> {
                it("should check the callCount and the query param of the mongo.findOne method", () -> {
                    // Init
                    String mockId = "mockId";
                    Query mockQuery = new Query();
                    mockQuery.addCriteria(Criteria.where("id").is(mockId));

                    // Action
                    phoneService.getPhone(mockId);

                    // Test
                    verify(mockMongoTemplate, times(1)).findOne(mockQuery, Phone.class, "phones");
                });
            });
        });
    }
}
