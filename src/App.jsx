import React, {Component} from "react";
import {Route, withRouter} from "react-router";

import "./App.css";
import HomePage from "./pages/HomePage";
import Store from "./stores/Store";
import PhonePage from "./pages/PhonePage";

class App extends Component {
    async componentDidMount() {
        await this.store.getPhones();
    }

    store = new Store();

    render() {
        return (
            <div className="app">
                <header className="app-header">
                    Telenor Webshop
                </header>
                <Route path="/phone/:id" render={props => <PhonePage {...props} store={this.store} id={props.match.params.id}/>}/>
                <Route exact path="/" render={props => <HomePage {...props} store={this.store}/>}/>
            </div>
        );
    }
}

export default withRouter(App);
