import {decorate, observable} from "mobx";
import RestAPI from "../rest/RestAPI";

export default class Store {
    rest = new RestAPI();

    phones = [];
    actualPageNumber = 0;
    phonePerPage = 5;
    numberOfPages = 0;

    filterList = {
        memorySize: [],
        simType: [],
        manufacturer: [],
        series: [],
        models: [],
    };

    memorySizeMap = {
        SMALL: "32 GB",
        MEDIUM: "64 GB",
        LARGE: "128 GB",
        EXTRALARGE: "256 GB",
    };

    async getPhone(id) {
        return this.rest.getPhone(id);
    }

    async getPhones() {
        const reply = await this.rest.getPhonesWithAllFilters(this.filterList, this.phonePerPage, this.actualPageNumber);
        this.phones = reply.phoneList;
        this.numberOfPages = reply.pageCount;
    }

    async getPhonesWithAllFilters() {
        const reply = await this.rest.getPhonesWithAllFilters(this.filterList, this.phonePerPage);
        this.phones = reply.phoneList;
        this.numberOfPages = reply.pageCount;
        this.actualPageNumber = 0;

        console.log("this.phones.length: ", this.phones.length);
        console.log("numberOfPages: ", this.numberOfPages);
    }
}

decorate(Store, {
    phones: observable,
    actualPageNumber: observable,
    phonePerPage: observable,
    numberOfPages: observable,
});
