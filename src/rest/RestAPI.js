import axios from "axios";

export default class RestAPI {
    constructor() {
        this.request = axios.create({
            baseURL: "http://localhost:8080",
            timeout: 10000,
            headers: {Accept: "application/json"},
        });
    }

    async getPhone(id) {
        return (await this.request.get(`/phone/${id}`)).data;
    }

    async getPhones(pageNumber, pageSize) {
        const pN = !pageNumber ? "" : pageNumber;
        const pS = !pageSize ? "" : pageSize;

        const url = `/phones?pageNumber=${pN}&pageSize=${pS}`;

        return (await this.request.get(url)).data;
    }

    async getPhonesWithAllFilters(filterList, pageSize, pageNumber) {
        const pN = !pageNumber ? "" : pageNumber;
        const pS = !pageSize ? "" : pageSize;
        const sim = filterList.simType.length === 0 ? "" : [...filterList.simType];
        const series = filterList.series.length === 0 ? "" : [...filterList.series];
        const memory = filterList.memorySize.length === 0 ? "" : [...filterList.memorySize];
        const manufacturer = filterList.manufacturer.length === 0 ? "" : [...filterList.manufacturer];

        const url = `/phones?pageNumber=${pN}&pageSize=${pS}&memorySize=${memory}&manufacturer=${manufacturer}&series=${series}&simType=${sim}`;

        console.log("url:", url);

        return (await this.request.get(url)).data;
    }

    async getCategories() {
        return (await this.request.get("/categories")).data;
    }
}
