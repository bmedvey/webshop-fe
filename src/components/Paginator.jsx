import React, {Component} from "react";
import {computed, decorate} from "mobx";
import {observer} from "mobx-react";
import PropTypes from "prop-types";

import "./paginator.css";

class Paginator extends Component {
    get nextButtonDisabled() {
        return this.store.actualPageNumber === this.store.numberOfPages;
    }

    get previousButtonDisabled() {
        return this.store.actualPageNumber === 0;
    }

    store = this.props.store;

    async handleNextPageClick() {
        if (this.store.actualPageNumber < this.store.numberOfPages) {
            this.store.actualPageNumber += 1;
            await this.props.store.getPhones();
        }
    }

    async handleLastPageClick() {
        this.store.actualPageNumber = this.store.numberOfPages;
        await this.props.store.getPhones();
    }

    async handleFirstPageClick() {
        this.store.actualPageNumber = 0;
        await this.props.store.getPhones();
    }

    async handlePreviousPageClick() {
        if (this.store.actualPageNumber >= 1) {
            this.store.actualPageNumber -= 1;
            await this.props.store.getPhones();
        }
    }

    async handlePageSizeChange(pageSize) {
        this.store.phonePerPage = pageSize;
        await this.props.store.getPhonesWithAllFilters();
    }

    renderPreviousButtons() {
        return <div className="previous-buttons">
            <button
                className="first"
                disabled={this.previousButtonDisabled}
                onClick={() => this.handleFirstPageClick()}
            >{"<<"}</button>
            <button
                className="last"
                disabled={this.previousButtonDisabled}
                onClick={() => this.handlePreviousPageClick()}
            >{"<"}</button>
        </div>;
    }

    renderNextButtons() {
        return <div className="next-buttons">
            <button
                className="next"
                disabled={this.nextButtonDisabled}
                onClick={() => this.handleNextPageClick()}
            >{">"}</button>
            <button
                className="last"
                disabled={this.nextButtonDisabled}
                onClick={() => this.handleLastPageClick()}
            >{">>"}</button>
        </div>;
    }

    renderPageSizeSelector() {
        return <div className="page-size-selector-container">
            Phones per page:
            <select onChange={e => this.handlePageSizeChange(e.target.value)}>
                <option value="5" >5</option>
                <option value="10">10</option>
                <option value="25">25</option>
                <option value="50">50</option>
            </select>
        </div>;
    }

    render() {
        return <div className="paginator">
            {this.renderPreviousButtons()}
            <div className="page-number-container">{`${this.store.actualPageNumber + 1} of ${this.store.numberOfPages + 1}`}</div>
            {this.renderNextButtons()}
            {this.renderPageSizeSelector()}
        </div>;
    }
}

Paginator.propTypes = {
    store: PropTypes.object.isRequired,
};

decorate(Paginator, {
    nextButtonDisabled: computed,
    previousButtonDisabled: computed,
});

export default observer(Paginator);
