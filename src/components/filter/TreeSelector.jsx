import React, {Component} from "react";
import {decorate, observable} from "mobx";
import {observer} from "mobx-react";
import PropTypes from "prop-types";

import RestAPI from "../../rest/RestAPI";
import "./treeselector.css";

class TreeSelector extends Component {
    async componentDidMount() {
        console.log("TreeSelector componentDidMount :");
        this.categories = await this.rest.getCategories();
    }

    categories = [];
    rest = new RestAPI();
    store = this.props.store;

    async modifyFilterList(type, e) {
        if (this.store.filterList[type].includes(e)) {
            const index = this.store.filterList[type].indexOf(e);
            this.store.filterList[type].splice(index, 1);
        } else {
            this.store.filterList[type].push(e);
        }
        await this.store.getPhonesWithAllFilters(this.store.filterList);
    }

    renderCheckBoxField(type, value) {
        return <input
            type="checkbox"
            value={value}
            onClick={e => this.modifyFilterList(type, e.target.value)}
        />;
    }

    renderPhoneList() {
        return <div className="selector-list">
            <div className="manufacturer-title">Gyártó/Széria:</div>
            {this.categories.manufacturerList.map(manufacturer =>
                <div className="manufacturer-list" key={manufacturer.manufacturer}>
                    {this.renderCheckBoxField("manufacturer", manufacturer.manufacturer)}
                    {manufacturer.manufacturer}:
                    {manufacturer.series.map(series =>
                        <div className="series-list" key={series}>
                            {this.renderCheckBoxField("series", series)}
                            {series}
                        </div>)}
                </div>)}

        </div>;
    }

    renderMemorySizes() {
        return <div className="memory-sizes">
            <div className="memory-title">Memória méret:</div>
            {this.categories.memorySizes.map(memorySize =>
                <div className="memory-list" key={memorySize}>
                    {this.renderCheckBoxField("memorySize", memorySize)}
                    {this.store.memorySizeMap[memorySize]}
                </div>)}
        </div>;
    }

    renderSimTypes() {
        return <div className="sim-types">
            <div className="sim-title">SIM kártyák száma:</div>
            {this.categories.simTypes.map(simType =>
                <div className="sim-list" key={simType}>
                    {this.renderCheckBoxField("simType", simType)}
                    {simType}
                </div>)}
        </div>;
    }

    renderFilterOptions() {
        return <div className="filter-options">
            {this.renderPhoneList()}
            {this.renderMemorySizes()}
            {this.renderSimTypes()}
        </div>;
    }

    render() {
        return <div className="tree-selector">
            {Object.keys(this.categories).length > 0 && this.renderFilterOptions()}
        </div>;
    }
}

TreeSelector.propTypes = {
    store: PropTypes.object.isRequired,
};

decorate(TreeSelector, {
    categories: observable,
});

export default observer(TreeSelector);
