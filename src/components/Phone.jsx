import React, {Component} from "react";
import PropTypes from "prop-types";

import "./phone.css";

class Phone extends Component {
    handleClick() {
        this.props.history.push(`phone/${this.props.phone.id}`);
    }

    render() {
        const thumbnail = `data:image/jpeg;base64, ${this.props.phone.thumbnail.data}`;
        return <div className="phone-item">
            <img className="phone-item-image" src={thumbnail} alt=""/>
            <h3>{this.props.phone.manufacturer} {this.props.phone.series} {this.props.phone.model}</h3>
            <div>{this.props.store.memorySizeMap[this.props.phone.memorySize]}, {this.props.phone.simType} SIM</div>
            <h3>Ár: {this.props.phone.price} Ft</h3>
            <button
                className="phone-button"
                onClick={() => this.handleClick()}>
                Megnézem
            </button>
        </div>;
    }
}

Phone.propTypes = {
    store: PropTypes.object.isRequired,
    phone: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
};

export default Phone;
