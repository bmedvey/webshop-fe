import React, {Component} from "react";
import PropTypes from "prop-types";
import {decorate, observable} from "mobx";
import {observer} from "mobx-react";

import "./phonepage.css";

class PhonePage extends Component {
    async componentDidMount() {
        this.phone = await this.props.store.getPhone(this.props.id);
    }

    phone;
    image;

    handleClick() {
        this.props.history.push("/");
    }

    render() {
        if (this.phone) {
            this.image = `data:image/jpeg;base64, ${this.phone.image.data}`;
        }
        console.log("PhonePage render :", this.props.id);
        return <div className="phone-page">
            {this.phone && <div className="phone-details">
                <div className="image">
                    <img className="phone-item-image" src={this.image} alt=""/>
                </div>
                <div className="description">
                    <h3>{this.phone.manufacturer} {this.phone.series} {this.phone.model}</h3>
                    <div>{this.phone.description}</div>
                    <p>{this.props.store.memorySizeMap[this.phone.memorySize]} memória, {this.phone.simType} SIM</p>
                    <h3>Ár: {this.phone.price} Ft</h3>
                    <button
                        className="phone-button"
                        onClick={() => this.handleClick()}>
                        Vissza
                    </button>
                </div>
            </div>}
        </div>;
    }
}

PhonePage.propTypes = {
    history: PropTypes.object.isRequired,
    store: PropTypes.object.isRequired,
    id: PropTypes.string.isRequired,
};

decorate(PhonePage, {
    phone: observable,
});

export default observer(PhonePage);
