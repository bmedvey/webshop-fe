import React, {Component} from "react";
import {observer} from "mobx-react";
import PropTypes from "prop-types";

import Phone from "../components/Phone";
import Paginator from "../components/Paginator";
import "./homepage.css";
import TreeSelector from "../components/filter/TreeSelector";

class HomePage extends Component {
    render() {
        return <div className="home-page">
            <TreeSelector store={this.props.store}/>
            <div className="phone-list">
                {this.props.store.phones.map((phone, id) => <Phone
                    phone={phone}
                    store={this.props.store}
                    key={id}
                    history={this.props.history}/>)}
            </div>
            <div className="paginator-container">
                <Paginator store={this.props.store}/>
            </div>
        </div>;
    }
}

HomePage.propTypes = {
    store: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
};

export default observer(HomePage);
